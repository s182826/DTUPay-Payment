package dk.dtu.payment.requests;

import dk.dtu.payment.model.PaymentDTO;
import dk.dtu.payment.model.RefundDTO;
import dk.dtu.payment.service.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import java.util.UUID;

/**
 * @author  Alexander V. Pedersen (s145099)
 */

@RestController
@RequestMapping("/api/payment")
public class RequestsResolver {
    private final PaymentService paymentService;

    @Autowired
    public RequestsResolver(PaymentService paymentService) {
        this.paymentService = paymentService;
    }

    @RequestMapping(value = "/pay", method = RequestMethod.POST)
    public void makePayment(@RequestBody PaymentDTO paymentDTO) {
        paymentService.transferMoney(
                paymentDTO.getDebtorCprNumber(),
                paymentDTO.getCreditorCprNumber(),
                UUID.fromString(paymentDTO.getTokenId()),
                paymentDTO.getAmount(),
                paymentDTO.getDescription());
    }

    @RequestMapping(value = "/refund", method = RequestMethod.POST)
    public void makeRefund(@RequestBody RefundDTO refundDTO) {
        paymentService.refundMoney(UUID.fromString(refundDTO.getTransactionUuid()), refundDTO.getDescription());
    }
}
