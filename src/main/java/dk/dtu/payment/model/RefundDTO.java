package dk.dtu.payment.model;

import java.io.Serializable;

/**
 * @author  s182826 (Kristoffer Thorø).
 */

public class RefundDTO implements Serializable {
    private String transactionUuid;
    private String description;

    // for deserialisation
    public RefundDTO() {}

    public RefundDTO(String transactionUuid, String description) {
        this.transactionUuid = transactionUuid;
        this.description = description;
    }

    public String getTransactionUuid() {
        return transactionUuid;
    }

    public void setTransactionUuid(String transactionUuid) {
        this.transactionUuid = transactionUuid;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
