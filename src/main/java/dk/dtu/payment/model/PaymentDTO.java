package dk.dtu.payment.model;

import java.io.Serializable;

/**
 * @author  s182826 (Kristoffer Thorø).
 */

public class PaymentDTO implements Serializable {

    private String debtorCprNumber;
    private String creditorCprNumber;
    private String tokenId;
    private double amount;
    private String description;

    public PaymentDTO(String debtorCprNumber, String creditorCprNumber, String tokenId, double amount, String description) {
        this.debtorCprNumber = debtorCprNumber;
        this.creditorCprNumber = creditorCprNumber;
        this.tokenId = tokenId;
        this.amount = amount;
        this.description = description;
    }

    public String getDebtorCprNumber() {
        return debtorCprNumber;
    }

    public void setDebtorCprNumber(String debtorCprNumber) {
        this.debtorCprNumber = debtorCprNumber;
    }

    public String getCreditorCprNumber() {
        return creditorCprNumber;
    }

    public void setCreditorCprNumber(String creditorCprNumber) {
        this.creditorCprNumber = creditorCprNumber;
    }

    public String getTokenId() {
        return tokenId;
    }

    public void setTokenId(String tokenId) {
        this.tokenId = tokenId;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
