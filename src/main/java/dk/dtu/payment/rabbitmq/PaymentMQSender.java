package dk.dtu.payment.rabbitmq;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author  s182826 (Kristoffer Thorø).
 */

@Component
public class PaymentMQSender {
    @Autowired
    private RabbitTemplate rabbitTemplate;

    public void sendPaymentMessage(String msg){
        System.out.println("Send msg = " + msg);
        rabbitTemplate.convertAndSend("deployment-request", "deployment-request-routing-key", msg);
    }

    public void sendRefundMessage(String msg){
        System.out.println("Send msg = " + msg);
        rabbitTemplate.convertAndSend("refund-exchange", "refund-exchange-routing-key", msg);
    }
}