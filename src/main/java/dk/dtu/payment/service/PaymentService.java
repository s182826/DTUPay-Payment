package dk.dtu.payment.service;

import com.google.gson.Gson;
import dk.dtu.payment.model.PaymentDTO;
import dk.dtu.payment.model.RefundDTO;
import dk.dtu.payment.rabbitmq.PaymentMQSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

/**
 * @author  s182826 (Kristoffer Thorø).
 */

@Service
public class PaymentService {
    private final PaymentMQSender paymentMQSender;

    @Autowired
    public PaymentService(PaymentMQSender paymentMQSender) {
        this.paymentMQSender = paymentMQSender;
    }

    public void transferMoney(String customerCprNumber, String merchantCprNumber, UUID tokenId, double amount, String description){
        Gson gson = new Gson();
        PaymentDTO PaymentDTO = new PaymentDTO(customerCprNumber,merchantCprNumber,tokenId.toString(),amount, description);
        String message = gson.toJson(PaymentDTO);
        paymentMQSender.sendPaymentMessage(message);
        System.out.println(" [x] Sent:'" + message + "'");
    }
    public void refundMoney(UUID transactionId, String description) {
        Gson gson = new Gson();
        RefundDTO refundDTO = new RefundDTO(transactionId.toString(), description);
        String message = gson.toJson(refundDTO);
        paymentMQSender.sendRefundMessage(message);
        System.out.println(" [x] Sent:'" + message + "'");
    }
}
