FROM adoptopenjdk:8-jre-hotspot
WORKDIR /usr/src
COPY target/payment-0.0.1-SNAPSHOT.jar /usr/src
EXPOSE 8083
CMD java -jar payment-0.0.1-SNAPSHOT.jar
